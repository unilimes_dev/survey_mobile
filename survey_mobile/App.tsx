import React from 'react';
import {Provider} from 'react-redux';
import store from './app/redux';
import AppNavigator from './AppNavigator';
import Reactotron from 'reactotron-react-native'
import ModalContainer from "./app/components/dialogs/modal";
import { useScreens } from 'react-native-screens';
useScreens();

import('./ReactotronConfig').then(() => {
    console.log('Reactotron Configured');
    Reactotron.log('hello rendering world')
}).catch((er) => {
    console.log(er);
    Reactotron.log('Error', er);

})

export default class App extends React.Component {

    render() {
        return (
            <Provider store={store}>
                <AppNavigator/>
                <ModalContainer/>
            </Provider>
        );
    }
}

import {createStackNavigator} from 'react-navigation'
// authentication views
import Register from '../app/views/auth/Register';
import Login from '../app/views/auth/Login';

const AuthNavigator = createStackNavigator(
    {
        Login: {screen: Login},
        Register: {screen: Register}
    },
    {
        headerMode: 'none'
    }
);

export default AuthNavigator;

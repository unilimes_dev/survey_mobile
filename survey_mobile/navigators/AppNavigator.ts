import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
// import Menu from '../app/views/main/Menu';
import Home from '../app/views/main/Home';
// create the navigators
const HomeStackNavigator = createStackNavigator({
    Home: { screen: Home },
});
const AppNavigator = createDrawerNavigator({
    Home: { screen: HomeStackNavigator },
},
    {
    // contentComponent: Menu,
    // drawerWidth: widthPercentageToDP('100%'),
    initialRouteName: 'Home'
}
);
export default AppNavigator;

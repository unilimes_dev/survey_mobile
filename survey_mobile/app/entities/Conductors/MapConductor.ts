export class MapConductor {
    protected tempElements: Array<any> = [];
    editable: boolean = false;
    draggable: boolean = false;
    clickable: boolean = false;

    constructor(protected map: any, editable: boolean = false, draggable: boolean = false, clickable: boolean = false) {
        this.editable = editable;
        this.draggable = draggable;
        this.clickable = true;
    }

    protected setMapOnAll(map: any, markers: any) {//clean or add to map
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    clean() {
        this.setMapOnAll(null, this.tempElements);
    }

    lastDrawedElement() {
        return this.tempElements[this.tempElements.length - 1];
    }
}

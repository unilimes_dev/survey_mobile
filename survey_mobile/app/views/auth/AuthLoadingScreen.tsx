import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {moduleName, loadUser, userSelector, applyHeader} from '../../ducks/auth';


interface MapProps {
    isChecked: boolean,
    loading: boolean,
    user: any,
    loadUser: Function,
    navigation: any
}

class AuthLoadingScreen extends React.Component<MapProps> {
    static defautlProps = {
        navigation: () => 1
    }

    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    componentWillReceiveProps(nextProps: Readonly<MapProps>, nextContext: any): void {

        if (nextProps.isChecked && nextProps.isChecked !== this.props.isChecked) {
            this.props.navigation.navigate(nextProps.user ? 'App' : 'Auth');
        }
    }

    _bootstrapAsync = async () => {
        const token = await AsyncStorage.getItem('access_token');
        await applyHeader(token);
        this.props.loadUser();
    };

    // Render any loading content that you like here
    render() {
        return (
            <View>
                <ActivityIndicator/>
                <StatusBar barStyle="default"/>
            </View>
        );
    }
}


const mapStateToProps = (state: any) => ({
    user: userSelector(state),
    authError: state[moduleName].error,
    isChecked: state[moduleName].isChecked,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        loadUser,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);


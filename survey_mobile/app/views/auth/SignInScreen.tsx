import React from "react";
import {
    TextInput,
    View,
    Button
} from "react-native";
import {
    NavigationParams,
    NavigationScreenProp,
    NavigationState
} from "react-navigation";
import {
    StyleSheet, Text
} from 'react-native';
// import Loading from '../../components/loading';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {signIn, moduleName, userSelector} from "../../ducks/auth";


interface MapProps {
    user: any,
    loading: any,
    authError: any,
    signIn: Function,
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface State {
    email: string,
    password: string
}

class SignInScreen extends React.Component<MapProps, State> {
    static navigationOptions = {
        title: 'Please sign in',
    };
    state = {
        pending: false,
        email: '',
        password: '',
    };

    componentWillReceiveProps(nextProps: Readonly<MapProps>, nextContext: any): void {
        if (nextProps.loading && nextProps.loading !== this.props.loading) {
            if (nextProps.user) this.props.navigation.navigate('App');
        }
    }

    render() {
        const {authError} = this.props;
        const buttonTitle = 'Sign in!';// + (this.state.pending ? (<Loading/>) : '');
        return (
            <View style={styles.container}>
                <TextInput
                    style={{height: 40}}
                    placeholder="Your email!"
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                />
                <TextInput
                    style={{height: 40}}
                    placeholder="Your password!"
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                />
                <Button
                    title={buttonTitle}
                    disabled={this.state.pending}
                    onPress={this._signInAsync}
                />
                {
                    authError && (<Text style={{color: 'red'}}>
                        Error! Either user or password are wrong. Please try again
                    </Text>)
                }
            </View>
        );
    }

    _signInAsync = async () => {
        this.props.signIn(this.state);
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});


const mapStateToProps = (state: any) => ({
    user: userSelector(state),
    authError: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        signIn,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);


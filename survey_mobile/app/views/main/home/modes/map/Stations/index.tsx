import React, {Fragment} from "react";
import {View} from "react-native";
import {createAppContainer, createStackNavigator, StackActions, NavigationActions} from 'react-navigation'; // Version can be specified in package.json
import {Marker} from 'react-native-maps';
import {Constants} from 'expo';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    locationParcelsSelector,
    locationSelector,
    locationsSelector,
    locationStationsSelector,
    moduleName
} from "../../../../../../ducks/map";
import {showDialogContent} from "../../../../../../ducks/dialogs";
import {Geometry, GPSCoordinate, Parcel, Station} from "../../../../../../entities";
import {searchSelector} from "../../../../../../ducks/auth";

interface MapProps {
    stations: Array<Station>,
    search: string,
    dateFilter: any,
    stationList: any,
    showStations: boolean,
}

class StationsElements extends React.Component<MapProps> {


    render() {
        const {
            stations,
        } = this.props;

        return (
            <Fragment>
                {
                    stations.map((marker: Station) => (
                        <Marker
                            key={marker.id}
                            coordinate={marker.points.toGPS()}
                        />
                    ))
                }
            </Fragment>
        );
    }
}

const mapStateToProps = (state: any) => ({
    search: searchSelector(state),
    mapCenter: state[moduleName].mapCenter,
    dateFilter: state[moduleName].dateFilter,
    stationList: state[moduleName].stationList,
    showStations: state[moduleName].showStations,
    stations: locationStationsSelector(state),
    error: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(StationsElements);


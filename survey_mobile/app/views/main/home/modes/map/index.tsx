import React, {Fragment} from "react";
import {StyleSheet, Text} from "react-native";
import {createAppContainer, createStackNavigator, StackActions, NavigationActions} from 'react-navigation'; // Version can be specified in package.json
import MapView from 'react-native-map-clustering';
import Toast, {DURATION} from 'react-native-easy-toast'
import {Marker, Polygon, Polyline} from 'react-native-maps';
import {Constants} from 'expo';
import Reactotron from 'reactotron-react-native'

import {connect} from 'react-redux';
import {
    lastGeoPostionsSelector, locationParcelsSelector,
    locationPoisSelector, locationPolesSelector, locationSegmentsSelector,
    locationStationsSelector,
    moduleName
} from "../../../../../ducks/map";
import {GPSCoordinate, Parcel, Poi, Pole, Segment, Station} from "../../../../../entities";
import StationIcon from '../../../../../assets/img/station.png';
import PoleIcon from '../../../../../assets/img/Pole.png';
import PoiIcon from '../../../../../assets/img/poi.png';
import {searchSelector} from "../../../../../ducks/auth";
import {segment_statuses, statuses} from "../../../../../utils";

interface MapProps {
    stations: Array<Station>,
    poles: Array<Pole>,
    segments: Array<Segment>,
    parcels: Array<Parcel>,
    pois: Array<Poi>,
    mapCenter: GPSCoordinate,
    search: string,
}

/*
* NOT able to split on smaller components
* */
class MapViewMode extends React.Component<MapProps> {

    private map: any;
    private searchCount: number = 0;
    private searchAvailabelCount: number = 0;

    componentDidMount(): void {


        Reactotron.log(this.map);
    }

    private filterItems(list: Array<any>, search: string) {
        if (!search) return list;
        let _list = [];
        const keys = list.length ? list[0].keys() : [];
        for (let i = 0; i < list.length; i++) {
            const el: any = list[i];
            if (search) {
                let isInseach = false;
                for (let j = 0; j < keys.length; j++) {
                    const val = el[keys[j]];
                    if (val && val.toString().toLowerCase().match(search.toLowerCase())) {
                        isInseach = true;
                        break;
                    }
                }
                if (!isInseach) continue;
            }
            _list.push(el);
        }
        if (_list.length === 0 && ++this.searchCount > this.searchAvailabelCount
        ) {
            // if (this.refs.toast) this.refs.toast.show('No elements fount on Map');
        }
        return _list;
    }

    private showDialog=(item)=>{
        console.log(item);
    }

    render() {

        const {props} = this;
        this.searchCount = 0;
        this.searchAvailabelCount = 0;
        [
            'showPois',
            'showParcels',
            'showSegments',
            'showStations',
            'showPois',
        ].forEach((el) => {
            if (props[el]) {
                this.searchAvailabelCount++;
            }
        });

        const {
            mapCenter,
            stations,
            pois,
            parcels,
            segments,
            poles,
            search,
        } = this.props;
        const _poles = this.filterItems(poles, search);
        const _segments = this.filterItems(segments, search);
        const _pois = this.filterItems(pois, search);
        const _stations = this.filterItems(stations, search);
        const _parcels = this.filterItems(parcels, search);


        return (
            <Fragment>
                <MapView
                    ref={ref => {
                        this.map = ref;
                    }}
                    region={{
                        ...mapCenter,
                        latitudeDelta: 8.5,
                        longitudeDelta: 8.5
                    }}
                    style={styles.container}
                >

                    {
                        _stations.map((marker: Station) => (
                            <Marker
                                key={marker.id}
                                coordinate={marker.points.toGPS()}
                                image={StationIcon}
                            />
                        ))
                    }
                    {
                        _poles.map((marker: Pole) => (
                            <Marker
                                key={marker.id}
                                coordinate={marker.points.toGPS()}
                                image={PoleIcon}
                            />
                        ))
                    }
                    {
                        _pois.map((marker: Poi) => (
                            <Marker
                                key={marker.id}
                                coordinate={marker.points.toGPS()}
                                image={PoiIcon}
                            />
                        ))
                    }
                    {
                        _parcels.map((marker: Parcel) => {
                            let strokeColor = '#000';
                            switch (marker.status) {
                                case segment_statuses[0].value: {
                                    strokeColor = 'blue';
                                    break;
                                }
                                case segment_statuses[1].value: {
                                    strokeColor = 'yellow';
                                    break;
                                }
                                case segment_statuses[2].value: {
                                    strokeColor = 'orange';
                                    break;
                                }
                                case segment_statuses[3].value: {
                                    strokeColor = 'red';
                                    break;
                                }
                                case segment_statuses[4].value: {
                                    strokeColor = 'green';
                                    break;
                                }
                                case segment_statuses[5].value: {
                                    strokeColor = 'grey';
                                    break;
                                }
                                case segment_statuses[6].value: {
                                    strokeColor = 'magenta';
                                    break;
                                }
                            }
                            return (
                                <Polygon
                                    key={marker.id}
                                    coordinates={marker.pathList}
                                    strokeWidth={2}
                                    strokeColor={strokeColor}
                                />
                            )
                        })
                    }
                    {
                        _segments.map((marker: Segment) => {
                            let strokeColor = marker.status === statuses[0].value ? 'blue' : (marker.status === statuses[1].value ? 'green' : 'red');

                            return (
                                <Polyline
                                    key={marker.id}
                                    coordinates={marker.pathList}
                                    strokeWidth={2}
                                    strokeColor={strokeColor}
                                />
                            )
                        })
                    }

                </MapView>
                <Toast
                    ref="toast"
                    style={{backgroundColor: 'red'}}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color: 'white'}}
                />
            </Fragment>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'stretch',
        height: '100%'
    }
});
const mapStateToProps = (state: any) => ({
    mapCenter: state[moduleName].mapCenter,
    stations: locationStationsSelector(state),


    // project: locationSelector(state),
    // selected_powerlines: powerlineSelector(state),
    search: searchSelector(state),
    mapZoom: state[moduleName].mapZoom,
    dateFilter: state[moduleName].dateFilter,
    allowAddPoi: state[moduleName].allowAddPoi,
    poiList: state[moduleName].poiList,
    segmentList: state[moduleName].segmentList,
    stationList: state[moduleName].stationList,
    polesList: state[moduleName].polesList,
    parcelList: state[moduleName].parcelList,
    showStations: state[moduleName].showStations,
    showSegments: state[moduleName].showSegments,
    showParcels: state[moduleName].showParcels,
    showPoles: state[moduleName].showPoles,
    showPois: state[moduleName].showPois,
    pois: locationPoisSelector(state),
    tempPosition: lastGeoPostionsSelector(state),
    segments: locationSegmentsSelector(state),
    poles: locationPolesSelector(state),
    parcels: locationParcelsSelector(state),
});
export default connect(mapStateToProps)(MapViewMode);


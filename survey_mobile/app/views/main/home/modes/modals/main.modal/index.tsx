import React, {Component} from 'react';

import {Form, Field} from 'react-native-validate-form';
import {View, Text, StyleSheet, AsyncStorage} from "react-native";
import Toast, {DURATION} from 'react-native-easy-toast'
import SelectInput from '@tele2/react-native-select-input';
// import UploadFile from '../../../../../components/upload';
import {
    Upload,
    Pole,
    Parcel, Segment, Station
} from "../../../../../../entities";
import {
    statuses,
    segment_statuses,
    required,
    email
} from "../../../../../../utils";
import Loading from "../../../../../../components/loading";
import InputField from "../../../../../../components/Input";
import SimpleInput from "../../../../../../components/Input/SimpleInput";
import {checkError} from "../../../../../../utils";

import styles from './styles';

interface MapProps {
    isAdmin: any,
    itemsList: any,
    position: any,
    selectedItem: any,
    location: any,
    tempPosition: Array<any>,
    onFinishEditItem: Function,
    changeContols: Function,
    editItem: Function,
    onDeleteItem: Function,
    onAddItem: Function,
    setDialogSaveButton: Function,
    showDialogContent: Function
}

interface MapState {
    uploads: Array<Upload>,
    errors: any,
    canDelete: boolean,
    __pending: boolean
}

export const TYPES = {
    NONE: -1,
    PARCEL: 1,
    POLE: 2,
    STATION: 3,
    SEGMENT: 4,
}

export default class MainModalDialog extends Component<MapProps, MapState> {


    protected title: string = '';
    protected type: number = TYPES.NONE;
    private myForm: any;
    static defaultProps: {
        itemsList: null,
        position: null,
        tempPosition: [],
        onAddItem: () => false,
        onDeleteItem: () => false,
        onFinishEditItem: () => false,
        changeContols: () => false
    };

    constructor(p: any) {
        super(p);
        this.state = {
            __pending: false,
            canDelete: false,
            errors: [],
            ...p.selectedItem
        }
    }
    componentDidMount(): void {
        this.props.setDialogSaveButton(
            (
                <View>
                    Save
                    {this.state.__pending && <Loading/>}
                </View>
            )
        )

    }

    componentWillReceiveProps(nextProps: any, nextContext: any): void {
        checkError(nextProps, this.props, () => 1, this.refs.toast);
        if (nextProps.itemsList !== this.props.itemsList) {
            this.setState({__pending: false});
            this.handleCancel({});
        }
    }

    private onUploadFile = (fileList: any) => {
        this.setState({
            uploads: [
                ...this.state.uploads,
                ...fileList
            ]
        })
    };
    private onUpdateFile = (fileList: any) => {
        this.setState({
            uploads: [
                ...fileList
            ]
        })
    };
    private onChange = (key: string) => {
        return (val: any) => {
            const newState: any = {
                [key]: val
            };
            this.setState(newState);
        }
    };


    protected handleOk = async (e: any) => {
        try {
            this.setState({__pending: true});
            await this.props.editItem({
                ...this.state,
            });
            this.props.onFinishEditItem();

        } catch (e) {
            toast.show(e.response ? e.response.data.error || e.response.data.message : e.meesage || e, {
                position: toast.POSITION.TOP_LEFT
            });
        } finally {
        }
    };

    protected handleCancel = (e: any) => {
        this.props.showDialogContent(null);
    };

    protected deleteItem = async (e: any) => {
        try {
            this.props.onDeleteItem({
                ...this.state,
            });
            this.props.onFinishEditItem();

        } catch (e) {
            console.log(e);
        }
        this.handleCancel(e);
        return false
    }

    private submitForm = () => {
        let submitResults = this.myForm.validate();

        let errors = [];

        submitResults.forEach(item => {
            errors.push({field: item.fieldName, error: item.error});
        });

        this.setState({errors: errors});
    }

    protected _render() {
        const state: any = this.state;
        const {title, comment}: any = this.state;
        const {selectedItem}: any = this.props;

        const fields = [];
        if (this.type === TYPES.PARCEL) {
            fields.push(
                {
                    title: 'Status',
                    name: 'status',
                    options: statuses
                },
                ...Parcel.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
        } else if (this.type === TYPES.POLE) {
            fields.push(
                ...Pole.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
        } else if (this.type === TYPES.SEGMENT) {
            fields.push(
                {
                    title: 'Status',
                    name: 'status',
                    options: segment_statuses
                },
                ...Segment.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
        } else if (this.type === TYPES.STATION) {
            fields.push(
                ...Station.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
        }
        const {isAdmin} = this.props;
        return (
            <View style={styles.container}>

                <View style={styles.scrollContent}>
                    <Form
                        ref={(ref) => this.myForm = ref}
                        validate={true}
                        submit={this.submitForm.bind(this)}
                        errors={this.state.errors}
                    >
                        {
                            fields.map((el: any) => {
                                if (el.options) {
                                    return (
                                        <SelectInput
                                            key={el.name}
                                            onChange={this.onChange(el.name)}
                                            style={{width: '100%'}}
                                            label={el.name}
                                            placeholder={el.name}
                                            value={state[el.name]}
                                            options={el.options.map((el: any) => ({
                                                label: el.text,
                                                value: el.value
                                            }))}
                                        />
                                    )
                                } else {
                                    return (
                                        <Field
                                            key={el.name}
                                            required
                                            component={InputField}
                                            validations={[required]}
                                            name={el.name}
                                            value={state[el.name]}
                                            disabled={!isAdmin}
                                            onChangeText={this.onChange(el.name)}
                                            customStyle={{width: '100%'}}
                                        />
                                    )
                                }
                            })
                        }

                        <Field
                            required
                            component={InputField}
                            validations={[]}
                            name={'title'}
                            value={title}
                            onChangeText={this.onChange('title')}
                            customStyle={{width: '100%'}}
                        />
                        <SimpleInput
                            multiline={true}
                            numberOfLines={4}
                            onChange={this.onChange('comment')}
                            value={comment}
                            name={'comment'}
                        />

                        {/* {
                            selectedItem.id && (
                                <UploadFile
                                    files={selectedItem.uploads}
                                    onUpload={this.onUploadFile}
                                    onUpdateFile={this.onUpdateFile}
                                />
                            )
                        }*/}
                    </Form>
                    );
                </View>
                <Toast
                    ref="toast"
                    style={{backgroundColor: 'bottom'}}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color: 'white'}}
                />
            </View>
        );
    }
}


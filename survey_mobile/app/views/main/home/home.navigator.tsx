import React from 'react';
import {createStackNavigator, createSwitchNavigator, createAppContainer} from 'react-navigation';
import ProjectList from './Modal.Projects';


const HomeStack = createStackNavigator({
    ModalProjects: {
        screen: ProjectList
    }
});

const HomeNavigator = createAppContainer(createSwitchNavigator(
    {
        HomeStack: HomeStack,
    },
    {
        initialRouteName: 'HomeStack',
    }
));
export default HomeNavigator;

import React, {Component} from 'react';
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    Platform,
    TouchableOpacity
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import PowerlineList from './index'
import {Project} from "../../../../entities";
import {locationSelector} from "../../../../ducks/map";

interface MapProps {
    showDialogContent: Function,
    project: Project
}

class PowerlinesListShow extends Component<MapProps> {

    private showItems = () => {
        this.props.showDialogContent(<PowerlineList/>)
    }

    render() {
        const styles: any = {paddingRight: 20};
        if (!this.props.project) {
            return null;
        }
        return (
            <TouchableOpacity onPress={this.showItems}>
                <Icon size={30}
                      style={styles}
                      name={Platform.OS === "ios" ? "ios-add" : "md-add"}
                />
            </TouchableOpacity>
        );
    }
}


const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
    }, dispatch)
);

const mapStateToProps = (state: any) => ({
    project: locationSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(PowerlinesListShow);


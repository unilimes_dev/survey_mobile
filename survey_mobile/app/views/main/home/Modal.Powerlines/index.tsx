import React, {Component} from 'react';
import {Platform, FlatList, StyleSheet, ScrollView, Text, View, TouchableHighlight} from 'react-native';

import {fetchLocations, selectLocation} from "../../../../ducks/map/locations";
import {
    moduleName,
    locationSelector,
    locationsSelector,
    changeContols,
    powerlinesSelector, powerlineSelector
} from "../../../../ducks/map";
import {fetchLocationStations} from "../../../../ducks/map/stations";
import {fetchLocationParcels} from "../../../../ducks/map/parcels";
import {fetchLocationPoles} from "../../../../ducks/map/poles";
import {fetchLocationSegments} from "../../../../ducks/map/segments";
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Powerline, Project} from "../../../../entities";

import CheckBox from '../../../../components/CheckBox'

interface MapProps {
    fetchLocationPoles: Function,
    fetchLocationParcels: Function,
    showDialogContent: Function,
    changeContols: Function,
    selected_powerlines: Array<number>,
    powerlines: Array<Powerline>,
    project: Project,
    projects: Array<Project>
}

interface Mapstate {
    isAll: boolean
}


class PowerlineList extends Component<MapProps, Mapstate> {

    state = {
        isAll: false
    };
    private loadItemData = (item: any) => {
        const reqData = {...this.props.project, powerLineId: item.id};
        // this.props.fetchLocationParcels(reqData);
        // this.props.fetchLocationPoles(reqData);
    }
    private selectItem = (item: any) => {
        let list: Array<number> = this.props.selected_powerlines;
        if (!item) {
            list = this.state.isAll ? [] : [...this.props.powerlines.map(el => el.id)];
            this.props.changeContols({
                name: 'selected_powerlines',
                value: [...list]
            });
            this.setState({
                isAll: !this.state.isAll
            });
            return this.props.powerlines.forEach((el: any) => {
                this.loadItemData(el);
            });
        }

        for (let i = 0; i < list.length; i++) {
            if (list[i] === item.id) {
                list.splice(i, 1);
                this.props.changeContols({
                    name: 'selected_powerlines',
                    value: [...list]
                });
                return;
            }
        }

        list.push(item.id);
        this.props.changeContols({
            name: 'selected_powerlines',
            value: [...list]
        });
        list.push(item.id);
        this.loadItemData(item);
    }
    private renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#CED0CE",
                }}
            />
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <Text>Select Powerline</Text>
                <View style={styles.scrollContent}>
                    <FlatList style={styles.scrollContent}
                              ItemSeparatorComponent={this.renderSeparator}

                              data={
                                  [
                                      {
                                          key: 'All',
                                          title: 'All'
                                      },
                                      ...this.props.powerlines
                                  ]
                              }
                              renderItem={({item, index, separators}: any) => {
                                  const selected = item.id ? (this.props.selected_powerlines.indexOf(item.id) > -1) : this.state.isAll;
                                  let styleItem = selected ? [styles.itemSelected] : [styles.item];
                                  return (
                                      <CheckBox
                                          onPress={() => this.selectItem(item.id ? item : null)}
                                          selected={selected}
                                          text={<Text style={styleItem}>{item.title}</Text>}
                                      />
                                  )
                              }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        width: '100%',
        minHeight: 120
    },
    scrollContent: {
        flex: 1,
        paddingTop: 5,
        width: '100%',
        minHeight: 120
    },
    item: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 0.5,
    },
    itemSelected: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 1,
    },
});

const mapStateToProps = (state: any) => ({
    powerlines: powerlinesSelector(state),
    selected_powerlines: powerlineSelector(state),
    project: locationSelector(state),
    error: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
        fetchLocationStations,
        fetchLocationParcels,
        fetchLocationPoles,
        fetchLocationSegments,
        showDialogContent,
        selectLocation,
        fetchLocations,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(PowerlineList);


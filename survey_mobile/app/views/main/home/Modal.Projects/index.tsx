import React, {Component} from 'react';
import {Platform, FlatList, StyleSheet, ScrollView, Text, View, TouchableHighlight} from 'react-native';
import {fetchLocations, selectLocation} from "../../../../ducks/map/locations";
import {moduleName, locationSelector, locationsSelector} from "../../../../ducks/map";
import {fetchLocationStations} from "../../../../ducks/map/stations";
import {fetchLocationPoi} from "../../../../ducks/map/poi";
import {fetchProjectPowerlines} from "../../../../ducks/map/powerlines";
import {fetchLocationSegments} from "../../../../ducks/map/segments";
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Project} from "../../../../entities";

interface MapProps {
    fetchLocationSegments: Function,
    fetchProjectPowerlines: Function,
    fetchLocationStations: Function,
    fetchLocationPoi: Function,
    showDialogContent: Function,
    selectLocation: Function,
    fetchLocations: Function,
    project: Project,
    projects: Array<Project>
}


class ProjectList extends Component<MapProps> {
    componentDidMount(): void {
        this.props.fetchLocations();
    }

    private selectProject = (project: any) => {
        this.props.selectLocation(project);
        this.props.showDialogContent(false);

        this.props.fetchLocationStations(project);
        // this.props.fetchLocationPoi(project);
        // this.props.fetchProjectPowerlines(project);
        // this.props.fetchLocationSegments(project);
    }
    private renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#CED0CE",
                }}
            />
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <Text>Select Project</Text>
                <View style={styles.scrollContent}>
                    <FlatList style={styles.scrollContent}
                              ItemSeparatorComponent={this.renderSeparator}

                              data={this.props.projects}
                              renderItem={({item, index, separators}) => {
                                  let styleItem = [styles.item];
                                  if (this.props.project && item.id === this.props.project.id) {
                                      styleItem = [styles.itemSelected];
                                  }
                                  return (
                                      <TouchableHighlight
                                          onPress={() => this.selectProject(item)}
                                          onShowUnderlay={separators.highlight}
                                          onHideUnderlay={separators.unhighlight}>
                                          <View style={{backgroundColor: 'white'}}>
                                              <Text style={styleItem}>{item.title}</Text>
                                          </View>
                                      </TouchableHighlight>
                                  )
                              }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        width: '100%',
        minHeight: 120
    },
    scrollContent: {
        flex: 1,
        paddingTop: 5,
        width: '100%',
        minHeight: 120
    },
    item: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 0.5,
    },
    itemSelected: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 1,
    },
});

const mapStateToProps = (state: any) => ({
    projects: locationsSelector(state),
    project: locationSelector(state),
    error: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        fetchLocationStations,
        fetchLocationPoi,
        fetchProjectPowerlines,
        fetchLocationSegments,
        showDialogContent,
        selectLocation,
        fetchLocations,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ProjectList);


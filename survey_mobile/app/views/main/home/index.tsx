import React from "react";
import {View, Text, StyleSheet, AsyncStorage} from "react-native";
import {createAppContainer, createStackNavigator, StackActions, NavigationActions} from 'react-navigation'; // Version can be specified in package.json
import {Constants} from 'expo';
import {
    Platform,
    TouchableOpacity
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

import MapViewMode from "./modes/map";

interface MapProps {
    navigation: any
}

export default class HomeScreen extends React.Component<MapProps> {
    static navigationOptions = {
        title: 'Map View',
        headerRight: (
            <TouchableOpacity onPress={() => alert('test')}>
                <Icon size={30}
                      style={{paddingRight: 20}}
                      name={Platform.OS === "ios" ? "ios-add" : "md-add"}
                />
            </TouchableOpacity>
        )
    };


    render() {
        return (
            <View style={styles.container}>
                <MapViewMode/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

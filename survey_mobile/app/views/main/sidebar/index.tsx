import React from "react";
import {View, Text, AsyncStorage} from "react-native";
import {
    createAppContainer,
    createStackNavigator,
    createDrawerNavigator,
    StackActions,
    NavigationActions
} from 'react-navigation'; // Version can be specified in package.json
import HomeScreen from '../home';

class LogOut extends React.Component {
    async componentDidMount(): void {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
    }

    render() {
        return null;
    }
}

const DrawerNavigator = createDrawerNavigator(
    {
        Home: HomeScreen,
        LogOut: LogOut,
    },
    {
        hideStatusBar: false,
        drawerBackgroundColor: 'rgba(255,255,255,.9)',
        overlayColor: '#6b52ae',
        contentOptions: {
            activeTintColor: '#fff',
            activeBackgroundColor: '#6b52ae',
        },
    }
);

export default createAppContainer(DrawerNavigator);

import React from 'react';
import {contentSelector, dialogSaveBtnSelector, showDialogContent} from "../../../ducks/dialogs";
import Modal from 'react-native-modal';
import {Text, TouchableOpacity, StyleSheet, View, Dimensions} from 'react-native';
import {
    Platform,
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import styles from './styles';

interface MapProps {
    showDialogContent: Function,
    dialogSaveBtn: any,
    content: any,
    alertText: any,
}

class ModalContainer extends React.Component <MapProps> {
    static defaultProps: {
        dialogSaveBtn:null
    }
    private handleClose = () => {
        this.props.showDialogContent(false);
    };
    private renderButton = (text, onPress) => (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text>{text}</Text>
            </View>
            {
                this.props.dialogSaveBtn
            }
        </TouchableOpacity>
    );

    private renderHeader = () => {
        return (
            <View style={styles.modalTitle}>
                <Text>{this.props.content.title}</Text>
                <TouchableOpacity onPress={this.handleClose}>
                    <Icon size={30}
                          style={{paddingRight: 20}}
                          name={Platform.OS === "ios" ? "ios-close" : "md-close"}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const {content}: any = this.props;
        return (
            <Modal
                isVisible={!!content}
                // backdropColor={'red'}
                // backdropOpacity={1}
                animationIn={'zoomInDown'}
                animationOut={'zoomOutUp'}
                animationInTiming={1000}
                animationOutTiming={1000}
                backdropTransitionInTiming={1000}
                backdropTransitionOutTiming={1000}
                style={{width: Dimensions.get('window').width * 0.9, height: Dimensions.get('window').height * 0.8}}
                onBackdropPress={this.handleClose}
            >
                <View style={styles.modalContent}>
                    {this.renderHeader()}
                    {content.content}
                    {this.renderButton('Close', this.handleClose)}
                </View>


            </Modal>
        )
    }
}

const mapStateToProps = (state: any) => ({
    content: contentSelector(state),
    dialogSaveBtn: dialogSaveBtnSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);

import {TextInput} from 'react-native';
import React, {Component} from 'react';

interface MapProps {
    multiline: any,
    numberOfLines: number,
    value: string,
    name: string,
    onChange: Function
}

export default class SimpleInput extends Component<MapProps> {
    render() {
        const {onChange}: any = this.props;
        return (
            <TextInput
                style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                onChangeText={onChange}
                value={this.props.value}
            />
        );
    }
}

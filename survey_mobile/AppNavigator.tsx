import React from 'react';
import {createStackNavigator, createSwitchNavigator, createAppContainer} from 'react-navigation';
import {View} from 'react-native';
import AuthLoadingScreen from './app/views/auth/AuthLoadingScreen';
import SignInScreen from './app/views/auth/SignInScreen';
import MainApp from './app/views/main';
import {
    Platform,
    TouchableOpacity
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import ProjectListShow from './app/views/main/home/Modal.Projects/show';
import PowerlinesListShow from './app/views/main/home/Modal.Powerlines/show';
import MapFilter from "./app/views/main/home/Map.Filter/show";

const AppStack = createStackNavigator({
    MainApp: {
        screen: MainApp,
        navigationOptions: ({navigation}) => ({
            title: "Menu",
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.openDrawer()}>
                    <Icon size={30}
                          style={{paddingLeft: 20}}
                          name={Platform.OS === "ios" ? "ios-menu-outline" : "md-menu"}
                    />
                </TouchableOpacity>
            ),
            headerRight: (
                <View style={{flex: 1, display: 'flex', flexDirection:'row'}}>
                    <ProjectListShow/>
                    <PowerlinesListShow/>
                    <MapFilter/>
                </View>
            ),
        })
    }
});
const AuthStack = createStackNavigator({SignIn: SignInScreen});

const AppNavigator = createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));
export default AppNavigator;


